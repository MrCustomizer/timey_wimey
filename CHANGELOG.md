# [1.3.0](https://gitlab.com/MrCustomizer/timey_wimey/compare/v1.2.0...v1.3.0) (2021-05-13)


### Features

* Update to ffi 1.0.0 ([49dda3d](https://gitlab.com/MrCustomizer/timey_wimey/commit/49dda3dc6549184027a5284cd4db89fe6b4d97ad)), closes [#22](https://gitlab.com/MrCustomizer/timey_wimey/issues/22)

# [1.2.0](https://gitlab.com/MrCustomizer/timey_wimey/compare/v1.1.0...v1.2.0) (2021-01-17)


### Bug Fixes

* configuration issues ([cb92348](https://gitlab.com/MrCustomizer/timey_wimey/commit/cb92348fcdb7be257da906be9b46b599b5eea3d7)), closes [#17](https://gitlab.com/MrCustomizer/timey_wimey/issues/17)


### Features

* add vlc as media player ([4bb56ef](https://gitlab.com/MrCustomizer/timey_wimey/commit/4bb56ef7d44883e987a0706cc577855943f0387e)), closes [#5](https://gitlab.com/MrCustomizer/timey_wimey/issues/5) [#6](https://gitlab.com/MrCustomizer/timey_wimey/issues/6) [#4](https://gitlab.com/MrCustomizer/timey_wimey/issues/4)

# [1.1.0](https://gitlab.com/MrCustomizer/timey_wimey/compare/v1.0.0...v1.1.0) (2020-12-16)


### Bug Fixes

* don't crash, when light sensor is missing ([b1d8ac3](https://gitlab.com/MrCustomizer/timey_wimey/commit/b1d8ac3f5c4c1d50a5f8b4f878da1d7d9baba7b5)), closes [#10](https://gitlab.com/MrCustomizer/timey_wimey/issues/10)
* Format single digit clock values correctly ([077643d](https://gitlab.com/MrCustomizer/timey_wimey/commit/077643d6d046f3ec5c132f7f34ba6c7ea976ff3c)), closes [#14](https://gitlab.com/MrCustomizer/timey_wimey/issues/14)


### Features

* add clock widget ([9412578](https://gitlab.com/MrCustomizer/timey_wimey/commit/94125784aa086457a2203e2652587d0fe74ddacf)), closes [#2](https://gitlab.com/MrCustomizer/timey_wimey/issues/2)
* add display dimming (hardware and software) ([8d03042](https://gitlab.com/MrCustomizer/timey_wimey/commit/8d03042350d1439f49aa304e03a068d6802b79a2)), closes [#11](https://gitlab.com/MrCustomizer/timey_wimey/issues/11)
* add gitlab releases to semrelease ([5fb4611](https://gitlab.com/MrCustomizer/timey_wimey/commit/5fb4611219364e76894bcb53e1a981ee797c62cc)), closes [#15](https://gitlab.com/MrCustomizer/timey_wimey/issues/15)
* add systemd file for autostart ([2c48d49](https://gitlab.com/MrCustomizer/timey_wimey/commit/2c48d4970b76f69a351c7c2bca597a0632a624fe)), closes [#13](https://gitlab.com/MrCustomizer/timey_wimey/issues/13)
* read lux value from light sensor ([2b0add0](https://gitlab.com/MrCustomizer/timey_wimey/commit/2b0add013a4234393c7be4b26a5d32a6e610237a)), closes [#1](https://gitlab.com/MrCustomizer/timey_wimey/issues/1)

# 1.0.0 (2020-11-29)


### Features

* add CI checks and release versioning ([2c055a7](https://gitlab.com/MrCustomizer/timey_wimey/commit/2c055a7740ed3cebf17250e19feb52baaa389de5)), closes [#8](https://gitlab.com/MrCustomizer/timey_wimey/issues/8)
