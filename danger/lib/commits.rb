def checkCommits(commits)
    messages = Array[]
    commits.each do |c|
        messages.push(c.message)
    end
    
    if (!containsAtLeastOneConventionalCommit(messages))
        failure('There has to be at least one conventional commit message in the MR.')
    end

    projectID = gitlab.mr_json['project_id']
    mrIid = gitlab.mr_json['iid']
    mrIssueNrs = []
    gitlab.api.merge_request_closes_issues(projectID, mrIid).each do |issue|
        mrIssueNrs.push(issue['iid'].to_s())
    end

    commits.each do |c|
        if (!isCommitMessageHeaderLengthOk(c.message))
            failure("Commit message header is longer than 50 characters: \n" + c.message +
                "\n\nCommit ID: " + c.sha)
        end
        if (!isCommitMessageBodyAndFooterLengthOk(c.message))
            failure("There is a line with more than 72 characters in the commit message: \n" + 
                + c.message + "\n\nCommit ID: " + c.sha)
        end

        commitIssueNr = getIssueNrFromMessage(c.message)

        if (isConventionalCommit(c.message))
            if (!mrIssueNrs.include?(commitIssueNr))
                failure("The following commit references issue #" + commitIssueNr +
                    " but the MR is not closing this issue:\n" + c.message +
                    "\n\nCommit ID: " + c.sha)
            end
        end
    end
end

def isCommitMessageHeaderLengthOk(message)
    lines = message.split(/\n/)

    if (lines[0].length > 50)
        return false
    end

    return true
end

def isCommitMessageBodyAndFooterLengthOk(message)
    lines = message.split(/\n/)

    lines.drop(1).each do |l|
        if (l.length > 72)
            return false
        end
    end

    return true
end

def containsAtLeastOneConventionalCommit(messages)
    messages.each do |m|
        if (isConventionalCommit(m))
            return true
        end
    end

    return false
end

def isConventionalCommit(message)
    # /x enters free spacing mode for regex (for multi line notation)
    regex = /\A(feat|fix|chore|docs|style|refactor|perf|test):\s.+
        ((\n(\n.+)+(\n\nIssue:?\s\#\d{1,9}))|
        (\n\nIssue:?\s\#\d{1,9}))\z/x

    return message.match?(regex)
end

def getIssueNrFromMessage(message)
    lines = message.split(/\n/)
    regex = /Issue:?\s#(?<issueNr>\d{1,9})/
    
    if matches = lines.last.match(regex)
        return matches[:issueNr].to_s()
    end

    return '[no_issue_nr]'
end