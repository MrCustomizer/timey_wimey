require_relative '../lib/commits'
require 'test/unit'
 
class GetIssueNrFromMessage < Test::Unit::TestCase
 
    def test_shouldReturn123_WhenLongMessageAndIssue123IsProvided
        message = 'docs: test commit message\n'\
            '\n'\
            'body line 1\n'\
            'body line 2\n'\
            'body line 3\n'\
            '\n'\
            'Issue: #123'

        assert_equal('123', getIssueNrFromMessage(message))
    end

    def test_shouldReturn44_WhenLongMessageAndIssue44WithoutColonIsProvided
        message = 'docs: test commit message\n'\
            '\n'\
            'body line 1\n'\
            'body line 2\n'\
            'body line 3\n'\
            '\n'\
            'Issue #44'

        assert_equal('44', getIssueNrFromMessage(message))
    end

    def test_shouldReturn3_WhenShortMessageAndIssue3IsProvided
        message = 'fix: test commit message\n'\
            '\n'\
            'Issue: #3'

        assert_equal('3', getIssueNrFromMessage(message))
    end

    def test_shouldReturn728_WhenShortMessageAndIssue728WithoutColonIsProvided
        message = 'fix: test commit message\n'\
            '\n'\
            'Issue #728'

        assert_equal('728', getIssueNrFromMessage(message))
    end
end