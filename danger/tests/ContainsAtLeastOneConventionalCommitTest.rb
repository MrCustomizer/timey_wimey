require_relative '../lib/commits'
require 'test/unit'
 
class ContainsAtLeastOneConventionalCommitTest < Test::Unit::TestCase
 
    def test_shouldReturnTrue_WhenAtLeastOneValidCommitMessageIsProvided
        validMessage = "fix: test commit message\n"\
            "\n"\
            "Issue: #123"

        invalidMessage = "bla: message"
        messages = [invalidMessage, validMessage]

        assert_true(containsAtLeastOneConventionalCommit(messages))
    end
      
    def test_shouldReturnTrue_WhenOneValidCommitMessageIsProvided
        validMessage = "fix: test commit message\n"\
            "\n"\
            "Issue: #123"

        messages = [validMessage]

        assert_true(containsAtLeastOneConventionalCommit(messages))
    end

    def test_shouldReturnFalse_WhenOnlyInvalidMessagesAreProvided
        invalidMessage1 = "burp: test commit message\n"\
            "\n"\
            "Issue: #abc"

        invalidMessage2 = "bla: message"
        messages = [invalidMessage1, invalidMessage2]

        assert_false(containsAtLeastOneConventionalCommit(messages))
    end
end
