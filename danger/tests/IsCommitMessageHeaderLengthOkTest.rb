require_relative '../lib/commits'
require 'test/unit'
 
class IsCommitMessageHeaderLengthOkTest < Test::Unit::TestCase
 
    def test_shouldReturnTrue_WhenHeaderWith50CharactersIsProvided
        message = "123456789 123456789 123456789 123456789 123456789 "\
            "\n"\
            "Issue: #123"
        assert_true(isCommitMessageHeaderLengthOk(message))
    end

    def test_shouldReturnFalse_WhenHeaderWith51CharactersIsProvided
        message = "123456789 123456789 123456789 123456789 123456789 1"\
            "\n"\
            "Issue: #123"
        assert_false(isCommitMessageHeaderLengthOk(message))
    end

   
end
