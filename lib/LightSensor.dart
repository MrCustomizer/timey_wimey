// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:ffi';
import 'package:ffi/ffi.dart';

typedef tsl2561_init = Pointer<Void> Function(
    Uint32 address, Pointer<Utf8> i2cDeviceFilepath);
typedef Tsl2561Init = Pointer<Void> Function(
    int address, Pointer<Utf8> i2cDeviceFilepath);

typedef tsl2561_lux = Int32 Function(Pointer<Void> tsl);
typedef Tsl2561Lux = int Function(Pointer<Void> tsl);

class LightSensor {
  final Tsl2561Lux tsl2561Lux;

  final Pointer<Void> i2cDevice;

  LightSensor._(this.tsl2561Lux, this.i2cDevice);

  factory LightSensor() {
    final DynamicLibrary dylib =
        DynamicLibrary.open('lib/tsl2561/build/libtsl2561.so');

    var initI2cDevice = dylib
        .lookup<NativeFunction<tsl2561_init>>('tsl2561_init')
        .asFunction<Tsl2561Init>();

    var device = initI2cDevice(0x39, '/dev/i2c-1'.toNativeUtf8());

    var lux = dylib
        .lookup<NativeFunction<tsl2561_lux>>('tsl2561_lux')
        .asFunction<Tsl2561Lux>();

    return LightSensor._(lux, device);
  }

  int getLux() {
    if (i2cDevice.address != nullptr.address) {
      return tsl2561Lux(i2cDevice);
    }

    // If we couldn't connect to the i2cDevice, we just return a value which
    // should be comparable to the maximum value of the light sensor.
    return 1000;
  }
}
