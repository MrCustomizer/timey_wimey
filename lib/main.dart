// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:timey_wimey/brightness/DimmerWidget.dart';
import 'package:timey_wimey/media_player/vlc/VlcWidget.dart';

import 'clock/Clock.dart';

void main(List<String> args) {
  runApp(ProviderScope(child: DimmerWidget(VlcWidget(Center(child: Clock())))));
}
