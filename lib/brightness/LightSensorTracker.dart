// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';
import 'dart:isolate';

import 'package:timey_wimey/LightSensor.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final lightSensorProvider = StateNotifierProvider<LightSensorTracker>(
    (ref) => new LightSensorTracker());

class LightSensorTracker extends StateNotifier<BrightnessModel> {
  static final _initialState = BrightnessModel(0);

  LightSensorTracker() : super(_initialState) {
    startTracking();
  }

  void startTracking() {
    var receivePort = ReceivePort();
    Isolate.spawn(_runTimer, receivePort.sendPort);
    receivePort.listen(_timerCallback);
  }

  static void _runTimer(SendPort sendPort) async {
    final LightSensor lightSensor = new LightSensor();
    Timer.periodic(Duration(milliseconds: 500), (Timer t) {
      var brightness = new BrightnessModel(lightSensor.getLux());
      sendPort.send(brightness);
    });
  }

  void _timerCallback(dynamic data) {
    state = data;
  }
}

class BrightnessModel {
  const BrightnessModel(this.brightness);
  final int brightness;
}
