// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:ffi';
import 'dart:io';

import 'package:timey_wimey/exceptions/UnmetDependencyException.dart';

typedef pigpio_start_func = Int32 Function(
    Pointer<Uint8> addrStr, Pointer<Uint8> portStr);
typedef PigpioStart = int Function(
    Pointer<Uint8> addrStr, Pointer<Uint8> portStr);

typedef pigpio_stop_func = Void Function(Int32 pi);
typedef PigpioStop = void Function(int pi);

typedef set_PWM_dutycycle_func = Int32 Function(
    Int32 pi, Uint32 gpio, Uint32 dutycycle);
typedef SetPWMDutycycle = int Function(int pi, int gpio, int dutycycle);

// Controls the brightness of the display via it's PWM control port
class Display {
  final int pi;

  final SetPWMDutycycle _setPWMDutycycle;

  Display._(this.pi, this._setPWMDutycycle);

  factory Display() {
    final libFile = '/usr/lib/libpigpiod_if2.so';
    if (!File(libFile).existsSync()) {
      throw new UnmetDependencyException(libFile, "libpigpiod");
    }

    var dylib = DynamicLibrary.open(libFile);

    var pigpioStart = dylib
        .lookup<NativeFunction<pigpio_start_func>>('pigpio_start')
        .asFunction<PigpioStart>();

    var setPWMDutycycle = dylib
        .lookup<NativeFunction<set_PWM_dutycycle_func>>('set_PWM_dutycycle')
        .asFunction<SetPWMDutycycle>();

    var pi = pigpioStart(nullptr, nullptr);

    return Display._(pi, setPWMDutycycle);
  }

  // Values from 0 to 255 are allowed
  void setBrightness(int brightness) {
    _setPWMDutycycle(pi, 18, brightness);
  }
}
