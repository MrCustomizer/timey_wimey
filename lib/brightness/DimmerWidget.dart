// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:timey_wimey/brightness/HardwareDimmerWidget.dart';

import 'LightSensorTracker.dart';

/// Dims the brightness of the display by modifying the opaqueness of an overlay
/// widget and the display's brightness control.
class DimmerWidget extends HookWidget {
  final HardwareDimmerWidget _hwDimmerWidget;

  factory DimmerWidget(Widget widget) {
    return DimmerWidget._(HardwareDimmerWidget(widget));
  }

  DimmerWidget._(this._hwDimmerWidget);

  @override
  Widget build(BuildContext context) {
    final animationController =
        useAnimationController(lowerBound: 100, upperBound: 255);
    final currentBrightness = useProvider(lightSensorProvider.state).brightness;

    var status = animationController.status;
    if (status != AnimationStatus.forward &&
        animationController.value != currentBrightness) {
      animationController.animateTo(currentBrightness.toDouble(),
          duration: Duration(milliseconds: 400));
    }

    return MaterialApp(
        home: Scaffold(
            backgroundColor: Colors.black,
            body: Center(
                child: FittedBox(
                    fit: BoxFit.contain,
                    child: DecoratedBox(
                        position: DecorationPosition.foreground,
                        decoration: BoxDecoration(
                            color: Color.fromARGB(
                                255 - useAnimation(animationController).toInt(),
                                0,
                                0,
                                0)),
                        child: _hwDimmerWidget)))));
  }
}
