// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/cupertino.dart';
import 'package:timey_wimey/brightness/Display.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:timey_wimey/exceptions/UnmetDependencyException.dart';

import 'LightSensorTracker.dart';

// Controls the displays brightness via its PWM control.
class HardwareDimmerWidget extends HookWidget {
  final _display;
  final _widget;

  HardwareDimmerWidget._(this._display, this._widget);

  factory HardwareDimmerWidget(Widget widget) {
    try {
      var display = new Display();
      return HardwareDimmerWidget._(display, widget);
    } on UnmetDependencyException catch (e) {
      print(e.message);
      return HardwareDimmerWidget._(null, widget);
    }
  }

  @override
  Widget build(BuildContext context) {
    final animationController =
        useAnimationController(lowerBound: 2, upperBound: 255);

    final currentBrightness = useProvider(lightSensorProvider.state).brightness;

    var status = animationController.status;
    if (status != AnimationStatus.forward &&
        animationController.value != currentBrightness) {
      animationController.animateTo(currentBrightness.toDouble(),
          duration: Duration(milliseconds: 400));
    }

    _updateBrightness(useAnimation(animationController));
    return _widget;
  }

  void _updateBrightness(double factor) {
    if (_display != null) {
      _display.setBrightness(factor.toInt());
    }
  }
}
