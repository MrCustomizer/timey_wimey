// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:timey_wimey/clock/TimeFormatter.dart';

final timeProvider = StateNotifierProvider((ref) => new TimeNotifier());

class TimeNotifier extends StateNotifier<String> {
  static final _initialState = '00:00';

  TimeNotifier() : super(_initialState) {
    Timer.periodic(Duration(milliseconds: 500), (Timer t) {
      state = new TimeFormatter().format(DateTime.now());
    });
  }
}
