// Copyright (C) 2020 Arne Augenstein
//
// This file is part of Timey Wimey.
//
// Timey Wimey is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Timey Wimey is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Timey Wimey.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';
import 'dart:io';

import 'package:timey_wimey/config/Configuration.dart';
import 'package:timey_wimey/media_player/MediaPlayer.dart';
import 'package:retry/retry.dart';

typedef Future<Socket> FutureGenerator<Socket>();

class VlcFactory {
  static Future<MediaPlayer> _instance;
  final _port = 9080;
  final _localhost = "localhost";

  Future<MediaPlayer> getOrCreateVlc() async {
    if (_instance == null) {
      /* 
       * There currently is no good way to do some cleanup when exiting the app,
       * so it's possible that there is an instance of VLC running from a 
       * previous run of the application. Try to connect to this instance
       * before creating a new one.
       * See also:
       * Issue #12 (https://gitlab.com/MrCustomizer/timey_wimey/-/issues/12)
       */
      try {
        // ignore until Issue #12 is resolved
        // ignore: close_sinks
        final socket = await Socket.connect(_localhost, _port);

        _instance = Future<MediaPlayer>.value(_Vlc(socket));
      } catch (e) {
        final args = [
          '-I',
          'rc',
          '--rc-host',
          '$_localhost:' + _port.toString()
        ];

        final config = Configuration();
        if (config.soundSystem == SoundSystem.alsa) {
          args.add('--aout=alsa');
          args.add('--alsa-audio-device=${config.soundDevice}');
        }

        await Process.start('cvlc', args);

        // ignore until Issue #12 is resolved
        // ignore: close_sinks
        final socket = await retry(
          () => Socket.connect(_localhost, _port),
          // Retry on SocketException or TimeoutException
          retryIf: (e) => e is SocketException || e is TimeoutException,
        );

        _instance = Future<MediaPlayer>.value(_Vlc(socket));
      }
    }

    return _instance;
  }
}

class _Vlc extends MediaPlayer {
  final Socket _socket;

  _PlayerState state = _PlayerState.Stopped;

  _Vlc(this._socket) {
    _socket.listen(_dataHandler, onDone: _doneHandler);
    _socket.setOption(SocketOption.tcpNoDelay, true);
  }

  @override
  void setStream(String url) {
    _sendCommand('add $url');
  }

  void _dataHandler(data) {
    print(new String.fromCharCodes(data).trim());
  }

  void _doneHandler() {
    _socket.destroy();
  }

  @override
  void play() {
    if (state == _PlayerState.Stopped) {
      state = _PlayerState.Playing;
      setStream('icyx://stream.radioparadise.com/mp3-192');
    } else {
      state = _PlayerState.Stopped;
      stop();
    }
  }

  @override
  void stop() {
    _sendCommand('stop');
  }

  void _sendCommand(String command) {
    _socket.write('$command\n');
  }
}

enum _PlayerState { Playing, Stopped }
