// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';
import 'package:timey_wimey/clock/TimeFormatter.dart';

void main() {
  test('Format single digit time values', () {
    var formatter = new TimeFormatter();

    var formattedTime =
        formatter.format(DateTime.parse("1969-07-20 02:08:04Z"));

    expect(formattedTime, '02:08');
  });

  test('Format double digit time values', () {
    var formatter = new TimeFormatter();

    var formattedTime =
        formatter.format(DateTime.parse("1969-07-20 21:28:04Z"));

    expect(formattedTime, '21:28');
  });
}
